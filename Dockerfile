# Front end Docker file. 

# Init argument 
ARG REPO=068549204303.dkr.ecr.us-east-1.amazonaws.com/nodebaseslim

# Get node base image
FROM ${REPO}:latest 

#Create a working directory
WORKDIR /Frontend

# Copy all package.json files to work directory. 
COPY package*.json ./

# Install dependancies 
RUN npm install

# Copy all files from FE to work directory
COPY . .

# Declare env
#ENV BACKEND_CONTAINER=ServiceBE.local

# Expose port 3000 of the container
EXPOSE 3000

# Run the docker script 
CMD ["npm", "start"]
#RUN npm run build