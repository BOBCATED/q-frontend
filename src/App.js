import './App.css';
import {Route , BrowserRouter , Redirect} from 'react-router-dom'
import Home from './pages/Home'
import Login from './pages/Login'
import Register from './pages/Register'
import QueuingBusiness from './pages/QueuingBusiness'
import 'antd/dist/antd.css';
import UserQueuings from './pages/UserQueuings';
import AddBusiness from './pages/AddBusiness';
import BusinessHome from './pages/BusinessHome';
import EditBusiness from './pages/EditBusiness';
import EditUser from './pages/EditUser';
import UserHome from './pages/UserHome';

function App() {
  return (
    <div className="App">
         <BrowserRouter>
             <ProtectedRoute path='/' exact component={Home} />
             <Route path='/login' exact component={Login} />
             <Route path='/register' exact component={Register} />
             <ProtectedRoute path='/queuing/:businessid' exact component={QueuingBusiness} />
             <ProtectedRoute path='/userqueuings' exact component={UserQueuings} />
             <ProtectedRoute path='/addbusiness' exact component={AddBusiness} />
             <ProtectedRoute path='/editbusiness/:businessid' exact component={EditBusiness} />
             <ProtectedRoute path='/business' exact component={BusinessHome} />
             <ProtectedRoute path='/edituser/:userid' exact component={EditUser} />
             <ProtectedRoute path='/user' exact component={UserHome} />
         </BrowserRouter>

    </div>
  );
}



export default App;


export function ProtectedRoute(props)
{


    if(localStorage.getItem('user'))
    {
      return <Route {...props}/>
    }
    else{
      return <Redirect to='/login'/>
    }

}
