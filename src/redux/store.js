import { createStore, applyMiddleware  , combineReducers } from 'redux';
import { composeWithDevTools } from 'redux-devtools-extension';
import thunk from 'redux-thunk';
import { alertsReducer } from './reducers/alertsReducer';
import { businessesReducer } from './reducers/businessesReducer';
import { queuingsReducer } from './reducers/queuingsReducer';
import { usersReducer } from './reducers/usersReducer';

const composeEnhancers = composeWithDevTools({});

const rootReducer = combineReducers({
   businessesReducer,
   alertsReducer,
   queuingsReducer,
   usersReducer,
})

const store = createStore(
  rootReducer,
  composeEnhancers(
    applyMiddleware(thunk)
   
  )
);

export default store