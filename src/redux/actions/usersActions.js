import axios from "axios";
import { message } from 'antd'

export const userLogin = (reqObj) => async dispatch => {

    dispatch({ type: 'LOADING', payload: true })

    try {
        const response = await axios.post('/api/users/login', reqObj, {
            headers: {
                'Q-API-KEY': 'dYrvpJQvu3OzNE3gKHNQJZ3BMSinLR1SgIOgUHXNBLrjmnDQj6IPuQjc6YNeLmeT' //API KEY
            }
        })
        localStorage.setItem('user', JSON.stringify(response.data))
        message.success('Login Success')
        dispatch({ type: 'LOADING', payload: false })
        const message_data = JSON.stringify(response.data)
        const message_result = JSON.parse(message_data)
        if (message_result.role === 'CUSTOMER' || message_result.role === 'ADMIN') {
            setTimeout(() => {
                window.location.href = '/'
            }, 500);
        }
        else if (message_result.role === 'MERCHANT') {
            setTimeout(() => {
                window.location.href = '/business'
            }, 500);
        }

    } catch (error) {
        // console.log(error.response.data)
        var errorMessage = error.response.data
        if (errorMessage != null) {
            message.error(errorMessage)
        }
        else {
            message.error('Something went wrong')
        }
        dispatch({ type: 'LOADING', payload: false })
    }
}

export const userRegister = (reqObj) => async dispatch => {

    dispatch({ type: 'LOADING', payload: true })

    try {
        const response = await axios.post('/api/users/register', reqObj, {
            headers: {
                'Q-API-KEY': 'dYrvpJQvu3OzNE3gKHNQJZ3BMSinLR1SgIOgUHXNBLrjmnDQj6IPuQjc6YNeLmeT' //API KEY
            }
        })
        console.log(response);
        message.success('Registration successfull')
        setTimeout(() => {
            window.location.href = '/login'

        }, 500);

        dispatch({ type: 'LOADING', payload: false })

    } catch (error) {
        // console.log(error)
        var errorMessage = error.response.data
        if (errorMessage != null) {
            message.error(errorMessage)
        }
        else {
            message.error('Something went wrong')
        }
        dispatch({ type: 'LOADING', payload: false })
    }
}

export const getAllUsers = () => async dispatch => {

    dispatch({ type: 'LOADING', payload: true })

    try {
        const response = await axios.get('/api/users/getallusers', {
            headers: {
                'Q-API-KEY': 'dYrvpJQvu3OzNE3gKHNQJZ3BMSinLR1SgIOgUHXNBLrjmnDQj6IPuQjc6YNeLmeT' //API KEY
            }
        })

        dispatch({ type: 'GET_ALL_USERS', payload: response.data })
        dispatch({ type: 'LOADING', payload: false })
        console.log(response);
    } catch (error) {
        console.log(error)
        dispatch({ type: 'LOADING', payload: false })
    }

}

export const editUser = (reqObj) => async dispatch => {

    dispatch({ type: 'LOADING', payload: true })

    try {
        await axios.post('/api/users/edituser', reqObj, {
            headers: {
                'Q-API-KEY': 'dYrvpJQvu3OzNE3gKHNQJZ3BMSinLR1SgIOgUHXNBLrjmnDQj6IPuQjc6YNeLmeT' //API KEY
            }
        })

        dispatch({ type: 'LOADING', payload: false })
        message.success('User details updated successfully')
        setTimeout(() => {
            window.location.href = '/user'
        }, 500);
    } catch (error) {
        console.log(error)
        dispatch({ type: 'LOADING', payload: false })
    }
}

export const deleteUser = (reqObj) => async dispatch => {

    dispatch({ type: 'LOADING', payload: true })

    try {
        await axios.post('/api/users/deleteuser', reqObj, {
            headers: {
                'Q-API-KEY': 'dYrvpJQvu3OzNE3gKHNQJZ3BMSinLR1SgIOgUHXNBLrjmnDQj6IPuQjc6YNeLmeT' //API KEY
            }
        })

        dispatch({ type: 'LOADING', payload: false })
        message.success('User deleted successfully')
        setTimeout(() => {
            window.location.reload()
        }, 500);
    } catch (error) {
        console.log(error)
        dispatch({ type: 'LOADING', payload: false })
    }


}