import { message } from 'antd';
import axios from 'axios';

export const getAllBusinesses = () => async dispatch => {

    dispatch({ type: 'LOADING', payload: true })

    try {
        const response = await axios.get('/api/businesses/getallbusinesses', {
            headers: {
                'Q-API-KEY': 'dYrvpJQvu3OzNE3gKHNQJZ3BMSinLR1SgIOgUHXNBLrjmnDQj6IPuQjc6YNeLmeT' //API KEY
            }
        })
        dispatch({ type: 'GET_ALL_BUSINESSES', payload: response.data })
        dispatch({ type: 'LOADING', payload: false })
    } catch (error) {
        console.log(error)
        dispatch({ type: 'LOADING', payload: false })
    }
}

export const addBusiness = (reqObj) => async dispatch => {

    dispatch({ type: 'LOADING', payload: true })

    try {
        await axios.post('/api/businesses/addbusiness', reqObj, {
            headers: {
                'Q-API-KEY': 'dYrvpJQvu3OzNE3gKHNQJZ3BMSinLR1SgIOgUHXNBLrjmnDQj6IPuQjc6YNeLmeT' //API KEY
            }
        })

        dispatch({ type: 'LOADING', payload: false })
        message.success('New business added successfully')
        setTimeout(() => {
            window.location.href = '/business'
        }, 500);
    } catch (error) {
        console.log(error)
        dispatch({ type: 'LOADING', payload: false })
    }


}

export const editBusiness = (reqObj) => async dispatch => {

    dispatch({ type: 'LOADING', payload: true })

    try {
        await axios.post('/api/businesses/editbusiness', reqObj, {
            headers: {
                'Q-API-KEY': 'dYrvpJQvu3OzNE3gKHNQJZ3BMSinLR1SgIOgUHXNBLrjmnDQj6IPuQjc6YNeLmeT' //API KEY
            }
        })

        dispatch({ type: 'LOADING', payload: false })
        message.success('Business details updated successfully')
        setTimeout(() => {
            window.location.href = '/business'
        }, 500);
    } catch (error) {
        console.log(error)
        dispatch({ type: 'LOADING', payload: false })
    }
}

export const deleteBusiness = (reqObj) => async dispatch => {

    dispatch({ type: 'LOADING', payload: true })

    try {
        await axios.post('/api/businesses/deletebusiness', reqObj, {
            headers: {
                'Q-API-KEY': 'dYrvpJQvu3OzNE3gKHNQJZ3BMSinLR1SgIOgUHXNBLrjmnDQj6IPuQjc6YNeLmeT' //API KEY
            }
        })

        dispatch({ type: 'LOADING', payload: false })
        message.success('Business deleted successfully')
        setTimeout(() => {
            window.location.reload()
        }, 500);
    } catch (error) {
        console.log(error)
        dispatch({ type: 'LOADING', payload: false })
    }


}