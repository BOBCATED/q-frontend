import axios from "axios";
import { message } from "antd";

export const queueBusiness = (reqObj) => async (dispatch) => {
  dispatch({ type: "LOADING", payload: true });

  try {
    await axios.post("/api/queuings/queuebusiness", reqObj, {
      headers: {
          'Q-API-KEY': 'dYrvpJQvu3OzNE3gKHNQJZ3BMSinLR1SgIOgUHXNBLrjmnDQj6IPuQjc6YNeLmeT' //API KEY
      }
  });

    dispatch({ type: "LOADING", payload: false });
    message.success("Your queue is added successfully");
    setTimeout(() => {
      window.location.href = '/userqueuings'
    }, 500);


  } catch (error) {
    console.log(error);
    dispatch({ type: "LOADING", payload: false });
    message.error("Something went wrong , please try later");
  }
};

export const getAllQueuings = () => async dispatch => {

  dispatch({ type: 'LOADING', payload: true })

  try {
    const response = await axios.get('/api/queuings/getallqueuings', {
      headers: {
          'Q-API-KEY': 'dYrvpJQvu3OzNE3gKHNQJZ3BMSinLR1SgIOgUHXNBLrjmnDQj6IPuQjc6YNeLmeT' //API KEY
      }
  })
    dispatch({ type: 'GET_ALL_QUEUINGS', payload: response.data })
    dispatch({ type: 'LOADING', payload: false })
  } catch (error) {
    dispatch({ type: 'LOADING', payload: false })
  }

}

export const deleteQueue = (reqObj) => async dispatch => {

  dispatch({ type: 'LOADING', payload: true })

  try {
    await axios.post('/api/queuings/deletequeue', reqObj, {
      headers: {
          'Q-API-KEY': 'dYrvpJQvu3OzNE3gKHNQJZ3BMSinLR1SgIOgUHXNBLrjmnDQj6IPuQjc6YNeLmeT' //API KEY
      }
  })

    dispatch({ type: 'LOADING', payload: false })
    message.success('Queue deleted successfully')
    setTimeout(() => {
      window.location.reload()
    }, 500);
  } catch (error) {
    console.log(error)
    dispatch({ type: 'LOADING', payload: false })
  }


}