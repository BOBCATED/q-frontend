const initialData = {
    businesses: [],
};

export const businessesReducer = (state = initialData, action) => {

    switch (action.type) {
        case 'GET_ALL_BUSINESSES': {
            return {
                ...state,
                businesses: action.payload
            }
        }

        default: return state
    }

}

