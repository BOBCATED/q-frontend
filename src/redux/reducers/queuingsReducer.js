const initialData = {
    queuings : [],

};

export const queuingsReducer = (state=initialData , action)=>{

     switch(action.type)
     {
         case 'GET_ALL_QUEUINGS' : {
             return{
                 ...state,
                 queuings : action.payload
             }
         }
         
         default:return state
     }

}
