import React from "react";
import { Row, Col } from "antd";
import { Link } from 'react-router-dom'

function DefaultLayout(props) {
  const user = JSON.parse(localStorage.getItem('user'))
  // const menu = (

  //   <Menu>
  //     <Menu.Item>
  //       <a
  //         href="/"
  //       >
  //         Home
  //       </a>
  //     </Menu.Item>
  //     <Menu.Item>
  //       <a
  //         href="/userqueues"
  //       >
  //         Queues
  //       </a>
  //     </Menu.Item>
  //     <Menu.Item>
  //       <a
  //         href="/business"
  //       >
  //         Business
  //       </a>
  //     </Menu.Item>
  //     <Menu.Item onClick={() => {
  //       localStorage.removeItem('user');
  //       window.location.href = '/login'
  //     }}>
  //       <li style={{ color: 'yellowgreen' }}>Logout</li>
  //     </Menu.Item>
  //   </Menu>
  // );
  if (user.role === 'CUSTOMER') {
    return (
      <div color="black">
        <div className="header bs1">
          <Row gutter={16} justify='center'>
            <Col lg={20} sm={24} xs={24}>
              <div className="d-flex justify-content-between">
                <h1 ><b><Link to='/' style={{ color: '#4299c1' }}>Q</Link></b></h1>
                <div class="dropdown">
                  <button class="dropbtn">{user.username}</button>
                  <div class="dropdown-content">
                    <a href="/">Home</a>
                    <a href="/userqueuings">Queues</a>
                    <a href="/login">Logout</a>
                  </div>
                </div>
              </div>
            </Col>
          </Row>
        </div>
        <div className="content">{props.children}</div>

        <br></br>
        <div className="footer text-center">
          <p>Q Systems</p>
        </div>
      </div>
    );
  }
  if (user.role === 'MERCHANT') {
    return (
      <div color="black">
        <div className="header bs1">
          <Row gutter={16} justify='center'>
            <Col lg={20} sm={24} xs={24}>
              <div className="d-flex justify-content-between">
                <h1 ><b><Link to='/business' style={{ color: '#4299c1' }}>Q</Link></b></h1>
                <div class="dropdown">
                  <button class="dropbtn">{user.username}</button>
                  <div class="dropdown-content">
                  <a href="/business">Manage Business</a>
                  <a href="/userqueuings">Manage Queue</a>
                    <a href="/login">Logout</a>
                  </div>
                </div>
              </div>
            </Col>
          </Row>
        </div>
        <div className="content">{props.children}</div>

        <br></br>
        <div className="footer text-center">
          <p>Q Systems</p>
        </div>
      </div>
    );
  }
  if (user.role === 'ADMIN') {
    return (
      <div color="black">
        <div className="header bs1">
          <Row gutter={16} justify='center'>
            <Col lg={20} sm={24} xs={24}>
              <div className="d-flex justify-content-between">
                <h1 ><b><Link to='/' style={{ color: '#4299c1' }}>Q</Link></b></h1>
                <div class="dropdown">
                  <button class="dropbtn">{user.username}</button>
                  <div class="dropdown-content">
                    <a href="/">Home</a>
                    <a href="/userqueuings">Manage Queues</a>
                    <a href="/business">Manage Business</a>
                    <a href="/user">Manage Users</a>
                    <a href="/login">Logout</a>
                  </div>
                </div>
              </div>
            </Col>
          </Row>

        </div>
        <div className="content">{props.children}</div>
        <br></br>
        <div className="footer text-center">
          <p>Q Systems</p>
        </div>
      </div>
    );
  }
}

export default DefaultLayout;
