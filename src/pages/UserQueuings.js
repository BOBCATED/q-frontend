import React, { useEffect } from "react";
import DefaultLayout from "../components/DefaultLayout";
import { useDispatch, useSelector } from "react-redux";
import { deleteQueue, getAllQueuings } from "../redux/actions/queuingActions";
import { Col, Row, Popconfirm } from "antd";
import Spinner from '../components/Spinner';
import moment from "moment";
import Helmet from 'react-helmet';
import { DeleteOutlined } from "@ant-design/icons";


function UserQueuings() {
  const dispatch = useDispatch();
  const { queuings } = useSelector((state) => state.queuingsReducer);
  const { loading } = useSelector((state) => state.alertsReducer);
  const user = JSON.parse(localStorage.getItem("user"));

  useEffect(() => {
    dispatch(getAllQueuings());
    // eslint-disable-next-line
  }, []);

  if (user.role === 'ADMIN') {
    return (
      <DefaultLayout>
        <Helmet>
          <style>{'body { background-color: black; }'}</style>
        </Helmet>
        {loading && (<Spinner />)}
        <h3 style={{ color: '#4299c1' }} className="text-center mt-2">All Queue</h3>
        <Row justify="center" gutter={16}>
          <Col lg={16} sm={24}>
            {queuings.filter(o => o.user).map((queuing) => {
              return <Row gutter={16} className="bs1 mt-3 text-left">
                <Col lg={6} sm={24}>
                  <p><b style={{ color: '#4299c1' }}>{queuing.business.name}</b></p>
                  {/* <p style={{ color: '#4299c1' }}>Total hours : <b>{queuing.totalHours}</b></p> */}
                  <p style={{ color: '#4299c1' }}>Deposit Amount : $<b>{queuing.business.depositAmount}</b></p>
                  <p style={{ color: '#4299c1' }}>Total Amount : $<b>{queuing.totalAmount / 100}</b></p>
                  <p style={{ color: '#4299c1' }}>Username : <b>{queuing.user.username}</b></p>
                  <p style={{ color: '#4299c1' }}>User Contact : <b>{queuing.user.email}</b></p>
                </Col>

                <Col lg={12} sm={24}>
                  <p style={{ color: '#4299c1' }}>Transaction ID : <b>{queuing.transactionId}</b></p>
                  <p style={{ color: '#4299c1' }}>Reservation Time: <b>{queuing.queuedTimeSlots.from}</b></p>
                  {/* <p style={{ color: '#4299c1' }}>To: <b>{queuing.queuedTimeSlots.to}</b></p> */}
                  <p style={{ color: '#4299c1' }}>Date of Queue: <b>{moment(queuing.createdAt).format('DD MMM yyyy')}</b></p>
                </Col>

                <Col lg={6} sm={24} className='text-right'>
                  <img style={{ borderRadius: 5 }} src={queuing.business.image} height="140" className="p-2" alt=""/>
                </Col>

                <Col lg={24} className='text-right'>
                  <Popconfirm
                    title="Are you sure to delete this queue?"
                    onConfirm={() => { dispatch(deleteQueue({ queueid: queuing._id })) }}

                    okText="Yes"
                    cancelText="No"
                  >
                    <DeleteOutlined
                      style={{ color: "red", cursor: "pointer" }}
                    />
                  </Popconfirm>
                </Col>
              </Row>;
            })}
          </Col>
        </Row>
      </DefaultLayout>
    );
  }
  else if (user.role === 'MERCHANT') {
    return (
      <DefaultLayout>
        <Helmet>
          <style>{'body { background-color: black; }'}</style>
        </Helmet>
        {loading && (<Spinner />)}
        <h3 style={{ color: '#4299c1' }} className="text-center mt-2">Current Queue</h3>
        <Row justify="center" gutter={16}>
          <Col lg={16} sm={24}>
            {queuings.filter(o => o.user).map((queuing) => {
              if (queuing.business.name === user.businessName) {
                return <Row gutter={16} className="bs1 mt-3 text-left">
                  <Col lg={6} sm={24}>
                    <p><b style={{ color: '#4299c1' }}>{queuing.business.name}</b></p>
                    {/* <p style={{ color: '#4299c1' }}>Total hours : <b>{queuing.totalHours}</b></p> */}
                    <p style={{ color: '#4299c1' }}>Deposit Amount : $<b>{queuing.business.depositAmount}</b></p>
                    <p style={{ color: '#4299c1' }}>Total Amount : $<b>{queuing.totalAmount / 100}</b></p>
                    <p style={{ color: '#4299c1' }}>Username : <b>{queuing.user.username}</b></p>
                    <p style={{ color: '#4299c1' }}>User Contact : <b>{queuing.user.email}</b></p>
                  </Col>

                  <Col lg={12} sm={24}>
                    <p style={{ color: '#4299c1' }}>Transaction ID : <b>{queuing.transactionId}</b></p>
                    <p style={{ color: '#4299c1' }}>Reservation Time: <b>{queuing.queuedTimeSlots.from}</b></p>
                    {/* <p style={{ color: '#4299c1' }}>To: <b>{queuing.queuedTimeSlots.to}</b></p> */}
                    <p style={{ color: '#4299c1' }}>Date of Queue: <b>{moment(queuing.createdAt).format('DD MMM yyyy')}</b></p>
                  </Col>

                  <Col lg={6} sm={24} className='text-right'>
                    <img style={{ borderRadius: 5 }} src={queuing.business.image} height="140" className="p-2" alt=""/>
                  </Col>

                  <Col lg={24} className='text-right'>
                    <Popconfirm
                      title="Are you sure to delete this queue?"
                      onConfirm={() => { dispatch(deleteQueue({ queueid: queuing._id })) }}

                      okText="Yes"
                      cancelText="No"
                    >
                      <DeleteOutlined
                        style={{ color: "red", cursor: "pointer" }}
                      />
                    </Popconfirm>
                  </Col>
                </Row>;
              }
              else
              {
                return null;
              }
            })}
          </Col>
        </Row>
      </DefaultLayout>
    );
  }
  else {
    return (
      <DefaultLayout>
        <Helmet>
          <style>{'body { background-color: black; }'}</style>
        </Helmet>
        {loading && (<Spinner />)}
        <h3 style={{ color: '#4299c1' }} className="text-center mt-2">My Queue</h3>
        <Row justify="center" gutter={16}>
          <Col lg={16} sm={24}>
            {queuings.filter(o => o.user).map((queuing) => {
              if (user._id === queuing.user._id)
              {
              return <Row gutter={16} className="bs1 mt-3 text-left">
                <Col lg={6} sm={24}>
                  <p><b style={{ color: '#4299c1' }}>{queuing.business.name}</b></p>
                  {/* <p style={{ color: '#4299c1' }}>Total hours : <b>{queuing.totalHours}</b></p> */}
                  <p style={{ color: '#4299c1' }}>Deposit Amount : $<b>{queuing.business.depositAmount}</b></p>
                  <p style={{ color: '#4299c1' }}>Total Amount : $<b>{queuing.totalAmount / 100}</b></p>
                  <p style={{ color: '#4299c1' }}>Username : <b>{queuing.user.username}</b></p>
                  <p style={{ color: '#4299c1' }}>User Contact : <b>{queuing.user.email}</b></p>

                </Col>

                <Col lg={12} sm={24}>
                  <p style={{ color: '#4299c1' }}>Transaction ID : <b>{queuing.transactionId}</b></p>
                  <p style={{ color: '#4299c1' }}>Reservation Time: <b>{queuing.queuedTimeSlots.from}</b></p>
                  {/* <p style={{ color: '#4299c1' }}>To: <b>{queuing.queuedTimeSlots.to}</b></p> */}
                  <p style={{ color: '#4299c1' }}>Date of Queue: <b>{moment(queuing.createdAt).format('DD MMM yyyy')}</b></p>
                </Col>

                <Col lg={6} sm={24} className='text-right'>
                  <img style={{ borderRadius: 5 }} src={queuing.business.image} height="140" className="p-2" alt=""/>
                </Col>
              </Row>;
              }
              else
              {
                return null;
              }
            })}
          </Col>
        </Row>
      </DefaultLayout>
    );
  }
}

export default UserQueuings;
