import React from 'react'
import { Row, Col, Form, Input } from 'antd'
import { Link } from 'react-router-dom'
import { useDispatch, useSelector } from 'react-redux'
import { userLogin } from '../redux/actions/usersActions'
import AOS from 'aos';
import Spinner from '../components/Spinner';
import 'aos/dist/aos.css'; // You can also use <link> for styles

AOS.init();
function Login() {
    const dispatch = useDispatch()
    const { loading } = useSelector(state => state.alertsReducer)
    function onFinish(values) {
        dispatch(userLogin(values))
        console.log(values)

    }
    return (
        <div className='login'>
            {loading && (<Spinner />)}
            <Row gutter={16} className='d-flex align-items-center' >

                <Col lg={14} style={{ position: 'relative' }}>
                    <img
                        className='w-100'
                        data-aos='slide-right'
                        data-aos-duration='1500'
                        src="../assets/images/svg/login.svg" 
                        alt=""
                        />
                </Col>
                <Col lg={10} className='text-left p-5'>
                    <Form layout='vertical' className='login-form p-4' onFinish={onFinish}>
                        <h1>Login to Q</h1>
                        <hr />
                        <Form.Item name='username' label='Username' rules={[{ required: true, message: "Please input your username!" }]} >
                            <Input />
                        </Form.Item>
                        <Form.Item name='password' label='Password' rules={[{ required: true, message: "Please input your password!" }]}>
                            <Input type='password' />
                        </Form.Item>
                        <button className='btn1 mt-2'>Login</button>
                        <hr />
                        <Link to='/register'>Click Here to Register</Link>
                    </Form>
                </Col>

            </Row>

        </div>

    )

}

export default Login