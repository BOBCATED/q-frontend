import { Col, Row, Form, Input, Radio } from 'antd';
import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import DefaultLayout from "../components/DefaultLayout";
import Spinner from "../components/Spinner";
import { editUser, getAllUsers } from "../redux/actions/usersActions";
import Helmet from 'react-helmet';
import { Link } from "react-router-dom";

function EditUser({ match }) {
  const { users } = useSelector((state) => state.usersReducer);
  const dispatch = useDispatch();
  const { loading } = useSelector((state) => state.alertsReducer);
  const [user, setuser] = useState();
  const [totalusers, settotalusers] = useState([]);
  const currentUser = JSON.parse(localStorage.getItem("user"));

  useEffect(() => {
    if (users.length === 0) {
      dispatch(getAllUsers());
    } else {
      settotalusers(users);
      setuser(users.find((o) => o._id === match.params.userid));
      console.log(user);
    }
    // eslint-disable-next-line
  }, [users]);

  function onFinish(values) {
    values._id = user._id;

    dispatch(editUser(values));
    console.log(values);
  }


  if (currentUser.role === 'ADMIN') {
    return (
      <DefaultLayout>
        <Helmet>
          <style>{'body { background-color: black; }'}</style>
        </Helmet>
        {loading && <Spinner />}
        <Row justify="center mt-5">
          <Col lg={12} sm={24} xs={24} className='p-2'>
            {totalusers.length > 0 && (
              <Form
                initialValues={user}
                className='login-form p-4'
                layout="vertical"
                onFinish={onFinish}
              >
                <h3 style={{ color: '#4299c1' }}>Edit User</h3>

                <hr />
                <Form.Item label={<label style={{ color: "#4299c1" }}>Username:</label>}
                  name="username"
                  rules={[{ required: true, message: "Please input username!" }]}
                >
                  <Input />
                </Form.Item>
                <Form.Item
                  name="email"
                  label={<label style={{ color: "#4299c1" }}>Email:</label>}
                  rules={[{ required: true, message: "Please input email!" }]}
                >
                  <Input />
                </Form.Item>
                <Form.Item name="role" label={<label style={{ color: "#4299c1" }}>User Role</label>} rules={[{ required: true, message: "Please input user role!" }]}>
                  <Radio.Group >
                  <Radio value="CUSTOMER" style={{ color: "#4299c1" }}>Customer</Radio>
                    <Radio value="MERCHANT" style={{ color: "#4299c1" }}>Merchant</Radio>
                    <Radio value="ADMIN" style={{ color: "#4299c1" }}>Admin</Radio>
                  </Radio.Group>
                </Form.Item>

                <Form.Item
                  name="businessName"
                  label={<label style={{ color: "#4299c1" }}>Business to Assiociate:</label>}
                >
                  <Input />
                </Form.Item>

                <div className="text-right">
                  <button className="btn1">Edit User</button>
                </div>
              </Form>
            )}
            <br></br>
            <div className='text-right'>
              <Link to="/user">Back</Link>
              <span>   </span>
            </div>
          </Col>
        </Row>
      </DefaultLayout>
    );
  }
}

export default EditUser;
