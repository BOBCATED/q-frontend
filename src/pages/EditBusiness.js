import { Col, Row, Form, Input, Radio } from 'antd';
import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import DefaultLayout from "../components/DefaultLayout";
import Spinner from "../components/Spinner";
import { editBusiness, getAllBusinesses } from "../redux/actions/businessesActions";
import Helmet from 'react-helmet';
import { Link } from "react-router-dom";

function EditBusiness({ match }) {
  const { businesses } = useSelector((state) => state.businessesReducer);
  const dispatch = useDispatch();
  const { loading } = useSelector((state) => state.alertsReducer);
  const [business, setbusiness] = useState();
  const [totalbusinesses, settotalbusinesses] = useState([]);
  const user = JSON.parse(localStorage.getItem("user"));

  useEffect(() => {
    if (businesses.length === 0) {
      dispatch(getAllBusinesses());
    } else {
      settotalbusinesses(businesses);
      setbusiness(businesses.find((o) => o._id === match.params.businessid));
      console.log(business);
    }
    // eslint-disable-next-line
  }, [businesses]);

  function onFinish(values) {
    values._id = business._id;

    dispatch(editBusiness(values));
    console.log(values);
  }


  if (user.role === 'ADMIN') {
    return (
      <DefaultLayout>
        <Helmet>
          <style>{'body { background-color: black; }'}</style>
        </Helmet>
        {loading && <Spinner />}
        <Row justify="center mt-5">
          <Col lg={12} sm={24} xs={24} className='p-2'>
            {totalbusinesses.length > 0 && (
              <Form
                initialValues={business}
                className='login-form p-4'
                layout="vertical"
                onFinish={onFinish}
              >
                <h3 style={{ color: '#4299c1' }}>Edit Business</h3>

                <hr />
                <Form.Item label={<label style={{ color: "#4299c1" }}>Business name</label>}
                  name="name"
                  rules={[{ required: true, message: "Please input your business name!" }]}
                >
                  <Input />
                </Form.Item>
                <Form.Item
                  name="image"
                  label={<label style={{ color: "#4299c1" }}>Image URL</label>}
                  rules={[{ required: true, message: "Please input an image URL!" }]}
                >
                  <Input />
                </Form.Item>
                <Form.Item
                  name="depositAmount"
                  label={<label style={{ color: "#4299c1" }}>Deposit Amount</label>}
                  rules={[{ required: true, message: "Please input your deposit amount!" }]}
                >
                  <Input />
                </Form.Item>
                <Form.Item
                  name="capacity"
                  label={<label style={{ color: "#4299c1" }}>Capacity</label>}
                  rules={[{ required: true, message: "Please input your business capacity!" }]}
                >
                  <Input />
                </Form.Item>
                <Form.Item name="businessType" label={<label style={{ color: "#4299c1" }}>Business Type</label>} rules={[{ required: true, message: "Please input your business type for your business!" }]}>
                  <Radio.Group >
                    <Radio value="Restaurants" style={{ color: "#4299c1" }}>Restaurants</Radio>
                    <Radio value="Services" style={{ color: "#4299c1" }}>Services</Radio>
                  </Radio.Group>
                </Form.Item>

                <div className="text-right">
                  <button className="btn1">Edit Business</button>
                </div>
              </Form>
            )}
            <br></br>
            <div className='text-right'>
              <Link to="/business">Back</Link>
              <span>   </span>
            </div>
          </Col>
        </Row>
      </DefaultLayout>
    );
  }
  else if (user.role === 'MERCHANT') {
    if (true) {
      return (
        <DefaultLayout>
          <Helmet>
            <style>{'body { background-color: black; }'}</style>
          </Helmet>
          {loading && <Spinner />}
          <Row justify="center mt-5">
            <Col lg={12} sm={24} xs={24} className='p-2'>
              {totalbusinesses.length > 0 && (
                <Form
                  initialValues={business}
                  className='login-form p-4'
                  layout="vertical"
                  onFinish={onFinish}
                >
                  <h3 style={{ color: '#4299c1' }}>Edit Business</h3>

                  <hr />
                  <Form.Item label={<label style={{ color: "#4299c1" }} >Business name</label>}
                    name="name"
                    rules={[{ required: true, message: "Please input your business name!" }]}
                  >
                    <Input readOnly />
                  </Form.Item>
                  <Form.Item
                    name="image"
                    label={<label style={{ color: "#4299c1" }}>Image URL</label>}
                    rules={[{ required: true, message: "Please input an image URL!" }]}
                  >
                    <Input />
                  </Form.Item>
                  <Form.Item
                    name="depositAmount"
                    label={<label style={{ color: "#4299c1" }}>Deposit amount</label>}
                    rules={[{ required: true, message: "Please input your deposit amount!" }]}
                  >
                    <Input />
                  </Form.Item>
                  <Form.Item
                    name="capacity"
                    label={<label style={{ color: "#4299c1" }}>Capacity</label>}
                    rules={[{ required: true, message: "Please input your business capacity!" }]}
                  >
                    <Input />
                  </Form.Item>
                  <Form.Item name="businessType" label={<label style={{ color: "#4299c1" }}>Business Type</label>} rules={[{ required: true, message: "Please input your business type for your business!" }]}>
                    <Radio.Group >
                      <Radio value="Restaurants" style={{ color: "#4299c1" }}>Restaurants</Radio>
                      <Radio value="Services" style={{ color: "#4299c1" }}>Services</Radio>
                    </Radio.Group>
                  </Form.Item>

                  <div className="text-right">
                    <button className="btn1">Edit Business</button>
                  </div>
                </Form>
              )}
              <br></br>
              <div className='text-right'>
                <Link to="/business">Back</Link>
                <span>   </span>
              </div>
            </Col>
          </Row>
        </DefaultLayout>
      );
    }
  }
}

export default EditBusiness;
