import React, { useEffect } from "react";
import DefaultLayout from "../components/DefaultLayout";
import { useDispatch, useSelector } from "react-redux";
import { deleteUser, getAllUsers } from "../redux/actions/usersActions";
import { Col, Row, Popconfirm } from "antd";
import Spinner from '../components/Spinner';
import Helmet from 'react-helmet';
import { DeleteOutlined, EditOutlined } from "@ant-design/icons";
import { Link } from "react-router-dom";


function UserHome() {
  const dispatch = useDispatch();
  const { users } = useSelector((state) => state.usersReducer);
  const { loading } = useSelector((state) => state.alertsReducer);
  const currentUser = JSON.parse(localStorage.getItem("user"));

  useEffect(() => {
    dispatch(getAllUsers());
    // eslint-disable-next-line
  }, []);

  if (currentUser.role === 'ADMIN') {
    return (
      <DefaultLayout>
        <Helmet>
          <style>{'body { background-color: black; }'}</style>
        </Helmet>
        {loading && (<Spinner />)}
        <h3 style={{ color: '#4299c1' }} className="text-center mt-2">All Users</h3>
        <Row justify="center" gutter={16}>
          <Col lg={16} sm={24}>
            {users.map((user) => {
              return <Row gutter={16} className="bs1 mt-3 text-left">
                <Col lg={6} sm={24}>
                  <p><b style={{ color: '#4299c1' }}> Username: {user.username}</b></p>
                  <p><b style={{ color: '#4299c1' }}> Email: {user.email}</b></p>
                </Col>
                <Col lg={12} sm={24}>
                  <p style={{ color: '#4299c1' }}>User Role : <b>{user.role}</b></p>
                  <p style={{ color: '#4299c1' }}>Allocated to : <b>{user.businessName}</b></p>
                </Col>

                <Col lg={20} className='text-right'>
                  <div className="mr-4">
                    <Link to={`/edituser/${user._id}`}>
                      <EditOutlined
                        className="mr-3"
                        style={{ color: "green", cursor: "pointer" }}
                      />
                    </Link>

                    <Popconfirm
                    title="Are you sure to delete this user?"
                    onConfirm={() => { dispatch(deleteUser({ userid: user._id })) }}

                    okText="Yes"
                    cancelText="No"
                  >
                    <DeleteOutlined
                      style={{ color: "red", cursor: "pointer" }}
                    />
                  </Popconfirm>
                  </div>


                </Col>
              </Row>;
            })}
          </Col>
        </Row>
      </DefaultLayout>
    );
  }
}

export default UserHome;
