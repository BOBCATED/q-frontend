import React, { useState, useEffect } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import DefaultLayout from '../components/DefaultLayout';
import { getAllBusinesses } from '../redux/actions/businessesActions';
import { Col, Row } from 'antd';
import { Link } from 'react-router-dom';
import Spinner from '../components/Spinner';
import Helmet from 'react-helmet';

//const { RangePicker } = DatePicker
function Home() {
    const { businesses } = useSelector(state => state.businessesReducer)
    const { loading } = useSelector(state => state.alertsReducer)
    const [totalBusinesses, setTotalbusinesses] = useState([])
    const dispatch = useDispatch()


    useEffect(() => {
        dispatch(getAllBusinesses())
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [])

    useEffect(() => {

        setTotalbusinesses(businesses)

    }, [businesses])


    // function setFilter(values) {

    //     var selectedFrom = moment(values[0], 'DD MMM yyyy HH:mm')
    //     var selectedTo = moment(values[1], 'DD MMM yyyy HH:mm')

    //     var temp = []

    //     for (var business of businesses) {

    //         if (business.queuedTimeSlots.length === 0) {
    //             temp.push(business)
    //         }
    //         else {

    //             for (var queuing of business.queuedTimeSlots) {

    //                 if (selectedFrom.isBetween(queuing.from, queuing.to) ||
    //                     selectedTo.isBetween(queuing.from, queuing.to) ||
    //                     moment(queuing.from).isBetween(selectedFrom, selectedTo) ||
    //                     moment(queuing.to).isBetween(selectedFrom, selectedTo)
    //                 ) {

    //                 }
    //                 else {
    //                     temp.push(business)
    //                 }

    //             }

    //         }

    //     }
    //     setTotalbusinesses(temp)
    // }

    return (
        <DefaultLayout>
            <Helmet>
                <style>{'body { background-color: black; }'}</style>
            </Helmet>

            {/* <Row className='mt-3' justify='center'>
                <Col lg={20} sm={24} className='d-flex justify-content-left'>
                    <RangePicker showTime={{ format: 'HH:mm' }} format='MMM DD yyyy HH:mm' onChange={setFilter} />
                </Col>
            </Row> */}

            {loading === true && (<Spinner />)}



            <Row justify='center' gutter={16}>

                {totalBusinesses.map(business => {
                    return <Col lg={5} sm={24} xs={24}>
                        <div className="business p-2 bs1">
                            <img src={business.image} className="businessimg" alt=""/>

                            <div className="business-content d-flex align-items-center justify-content-between">

                                <div className='text-left pl-2'>
                                    <p>{business.name}</p>
                                    <p> Deposit Amount: {business.depositAmount}</p>
                                    <p> In Queue: {totalBusinesses.length}</p>
                                </div>

                                <div>
                                    <button className="btn1 mr-2"><Link to={`/queuing/${business._id}`}>Queue Now</Link></button>
                                </div>

                            </div>
                        </div>
                    </Col>
                })}

            </Row>

        </DefaultLayout>
    )
}

export default Home
