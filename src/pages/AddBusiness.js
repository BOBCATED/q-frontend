import { Col, Row, Form, Input, Radio } from 'antd';
import React from 'react';
import { useDispatch, useSelector } from 'react-redux';
import DefaultLayout from '../components/DefaultLayout';
import Spinner from '../components/Spinner';
import { addBusiness } from '../redux/actions/businessesActions';
import Helmet from 'react-helmet';
import { Link } from "react-router-dom";

function AddBusiness() {

    const dispatch = useDispatch()
    const { loading } = useSelector(state => state.alertsReducer)
    const user = JSON.parse(localStorage.getItem("user"));

    function onFinish(values) {

        values.queuedTimeSlots = []

        dispatch(addBusiness(values))
        console.log(values)
    }

    if (user.role === 'ADMIN') {

        return (
            <DefaultLayout>
                <Helmet>
                    <style>{'body { background-color: black; }'}</style>
                </Helmet>
                {loading && (<Spinner />)}
                <Row justify='center mt-5'>
                    <Col lg={12} sm={24} xs={24} className='p-2'>
                        <Form className='bs1 p-2' layout='vertical' onFinish={onFinish}>
                            <h3 style={{ color: '#4299c1' }}>Add New Business</h3>
                            <hr />
                            <Form.Item name='name' label={<label style={{ color: "#4299c1" }}>Business Name</label>} rules={[{ required: true, message: "Please input your Business name!" }]}>
                                <Input />
                            </Form.Item>
                            <Form.Item name='image' label={<label style={{ color: "#4299c1" }}>Image URL</label>} rules={[{ required: true, message: "Please input your an image for your business!" }]}>
                                <Input />
                            </Form.Item>
                            <Form.Item name='depositAmount' label={<label style={{ color: "#4299c1" }}>Deposit Amount</label>} rules={[{ required: true, message: "Please input Deposit amount!" }]}>
                                <Input />
                            </Form.Item>
                            <Form.Item name='capacity' label={<label style={{ color: "#4299c1" }}>Capacity</label>} rules={[{ required: true, message: "Please input your capacity for your business!" }]}>
                                <Input />
                            </Form.Item>
                            <Form.Item name="businessType" label={<label style={{ color: "#4299c1" }}>Business Type</label>} rules={[{ required: true, message: "Please input your business type for your business!" }]}>
                                <Radio.Group >
                                    <Radio value="Restaurants" style={{ color: "#4299c1" }}>Restaurants</Radio>
                                    <Radio value="Services" style={{ color: "#4299c1" }}>Services</Radio>
                                </Radio.Group>
                            </Form.Item>

                            <div className='text-right'>
                                <button className='btn1'>ADD BUSINESS</button>
                            </div>
                        </Form>
                        <br></br>
                        <div className='text-right'>
                            <Link to="/business">Back</Link>
                            <span>   </span>
                        </div>
                    </Col>
                </Row>

            </DefaultLayout >
        )
    }
}

export default AddBusiness
