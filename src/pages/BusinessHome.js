import React, { useState, useEffect } from "react";
import { useSelector, useDispatch } from "react-redux";
import DefaultLayout from "../components/DefaultLayout";
import { deleteBusiness, getAllBusinesses } from "../redux/actions/businessesActions";
import { Col, Row, Popconfirm } from "antd";
import { Link } from "react-router-dom";
import Spinner from "../components/Spinner";
import { DeleteOutlined, EditOutlined } from "@ant-design/icons";
import Helmet from 'react-helmet';


function BusinessHome() {
  const { businesses } = useSelector((state) => state.businessesReducer);
  const { loading } = useSelector((state) => state.alertsReducer);
  const [totalBusinesses, setTotalbusinesses] = useState([]);
  const dispatch = useDispatch();
  const user = JSON.parse(localStorage.getItem("user"));

  useEffect(() => {
    dispatch(getAllBusinesses());
    // eslint-disable-next-line
  }, []);

  useEffect(() => {
    setTotalbusinesses(businesses);
  }, [businesses]);

  if (user.role === 'ADMIN') {
    return (
      <DefaultLayout>
        <Helmet>
          <style>{'body { background-color: black; }'}</style>
        </Helmet>
        <Row justify="center" gutter={16} className="mt-2">
          <Col lg={20} sm={24}>
            <div className="d-flex justify-content-between align-items-center">
              <h3 style={{ color: '#4299c1' }} className="mt-1 mr-2">Business Panel</h3>
              <button className="btn1">
                <a href="/addbusiness">Add a Business</a>
              </button>
            </div>
          </Col>
        </Row>

        {loading === true && <Spinner />}

        <Row justify="center" gutter={16}>
          {totalBusinesses.map((business) => {
            return (
              <Col lg={5} sm={24} xs={24}>
                <div className="business p-2 bs1">
                  <img src={business.image} className="businessimg" alt="" />

                  <div className="business-content d-flex align-items-center justify-content-between">
                    <div className="text-left pl-2">
                      <p>{business.name}</p>
                      <p> Deposit Amount: {business.depositAmount}</p>
                    </div>

                    <div className="mr-4">
                      <Link to={`/editbusiness/${business._id}`}>
                        <EditOutlined
                          className="mr-3"
                          style={{ color: "green", cursor: "pointer" }}
                        />
                      </Link>

                      <Popconfirm style={{ color: "#4299c1" }}
                        title="Are you sure to delete this business?"
                        onConfirm={() => { dispatch(deleteBusiness({ businessid: business._id })) }}
                        okText="Yes"
                        cancelText="No"
                      >
                        <DeleteOutlined
                          style={{ color: "red", cursor: "pointer" }}
                        />
                      </Popconfirm>
                    </div>
                  </div>
                </div>
              </Col>
            );
          })}
        </Row>
      </DefaultLayout>
    );
  }
  else if (user.role === 'MERCHANT') {
    return (
      <DefaultLayout>
        <Helmet>
          <style>{'body { background-color: black; }'}</style>
        </Helmet>
        <Row justify="center" gutter={16} className="mt-2">
          <Col lg={20} sm={24}>
            <div className="d-flex justify-content-between align-items-center">
              <h3 style={{ color: '#4299c1' }} className="mt-1 mr-2">Manage Your Business</h3>
            </div>
          </Col>
        </Row>

        {loading === true && <Spinner />}

        <Row justify="center" gutter={16}>
          {totalBusinesses.map((business) => {
            if (business.name === user.businessName) {
              return (
                <Col lg={5} sm={24} xs={24}>
                  <div className="business p-2 bs1">
                    <img src={business.image} className="businessimg" alt="" />

                    <div className="business-content d-flex align-items-center justify-content-between">
                      <div className="text-left pl-2">
                        <p>{business.name}</p>
                        <p> Deposit Amount: {business.depositAmount}</p>
                      </div>

                      <div className="mr-4">
                        <Link to={`/editbusiness/${business._id}`}>
                          <EditOutlined
                            className="mr-3"
                            style={{ color: "green", cursor: "pointer" }}
                          />
                        </Link>

                        <Popconfirm
                          title="Are you sure to delete this business?"
                          onConfirm={() => { dispatch(deleteBusiness({ businessid: business._id })) }}
                          okText="Yes"
                          cancelText="No"
                        >
                          <DeleteOutlined
                            style={{ color: "red", cursor: "pointer" }}
                          />
                        </Popconfirm>
                      </div>
                    </div>
                  </div>
                </Col>
              );
            }
            else
            {
              return null;
            }
          }
          )}
        </Row>
      </DefaultLayout>
    );
  }
}

export default BusinessHome;
