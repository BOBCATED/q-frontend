import React from "react";
import { Row, Col, Form, Input, Radio } from "antd";
import { Link } from "react-router-dom";
import { useDispatch, useSelector } from 'react-redux'
import { userRegister } from "../redux/actions/usersActions";
import AOS from 'aos';
import Spinner from '../components/Spinner';
import 'aos/dist/aos.css'; // You can also use <link> for styles
// ..
AOS.init()
function Register() {
  const dispatch = useDispatch()
  const { loading } = useSelector(state => state.alertsReducer)
  function onFinish(values) {
    dispatch(userRegister(values))
    console.log(values)
  }



  return (
    <div className="login">
      {loading && (<Spinner />)}
      <Row gutter={16} className="d-flex align-items-center">
        <Col lg={13} style={{ position: "relative" }}>
          <img
            className='w-100'
            data-aos='slide-left'
            data-aos-duration='1500'
            src="../assets/images/svg/register.svg" 
            alt = ""/>
        </Col>
        <Col lg={11} className="text-left p-5">
          <Form layout="vertical" className="login-form p-4" onFinish={onFinish} >
            <h1>Register</h1>
            <hr />
            <Form.Item
              name="username"
              label="Username"
              rules={[{ required: true, message: "Please input your username!" }]}
            >
              <Input />
            </Form.Item>
            <Form.Item
              name="email"
              label="Email"
              rules={[{ required: true, message: "Please input your email!" }]}
            >
              <Input />
            </Form.Item>
            <Form.Item
              name="password"
              label="Password"
              rules={[{ required: true, message: "Please input your password!" }]}
            >
              <Input type='password' />
            </Form.Item>
            <Form.Item
              name="cpassword"
              label="Confirm Password"
              rules={[{ required: true, message: "Please check your password!" }]}
            >
              <Input type='password' />
            </Form.Item>
            <Form.Item name="role" label="Registering as" rules={[{ required: true, message: "Please input select role!" }]}>
              <Radio.Group>
                <Radio value="MERCHANT">Merchant</Radio>
                <Radio value="CUSTOMER">Customer</Radio>
              </Radio.Group>
            </Form.Item>

            <button className="btn1 mt-2 mb-3">Register</button>
            <br />

            <Link to="/login">Click Here to Login</Link>
          </Form>
        </Col>
      </Row>
    </div >
  );
}

export default Register;
