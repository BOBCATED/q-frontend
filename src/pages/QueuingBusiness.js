import { Col, Row, Divider, DatePicker, Checkbox, Modal } from "antd";
import React, { useState, useEffect } from "react";
import { useSelector, useDispatch } from "react-redux";
import DefaultLayout from "../components/DefaultLayout";
import Spinner from "../components/Spinner";
import { getAllBusinesses } from "../redux/actions/businessesActions";
import moment from "moment";
import { queueBusiness } from "../redux/actions/queuingActions";
import StripeCheckout from "react-stripe-checkout";
import Helmet from 'react-helmet';
import { Link } from "react-router-dom";

import 'aos/dist/aos.css'; // You can also use <link> for styles
function QueuingBusiness({ match }) {
  const { businesses } = useSelector((state) => state.businessesReducer);
  const { loading } = useSelector((state) => state.alertsReducer);
  const [business, setbusiness] = useState({});
  const dispatch = useDispatch();
  const [from, setFrom] = useState(0);
  const [to, setTo] = useState(0);
  const [totalHours, setTotalHours] = useState(0);
  const [priority, setpriority] = useState(false);
  const [totalAmount, setTotalAmount] = useState(0);
  const [showModal, setShowModal] = useState(false);

  useEffect(() => {
    if (businesses.length === 0) {
      dispatch(getAllBusinesses());
    } else {
      setbusiness(businesses.find((o) => o._id === match.params.businessid));
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [businesses]);

  useEffect(() => {
    setTotalAmount(100 * business.depositAmount);
    if (priority) {
      setTotalAmount(totalAmount + 100);
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [priority, totalHours]);

  // function selectTimeSlots(values) {
  //   setFrom(moment(values[0]).format("DD MMM yyyy HH:mm"));
  //   setTo(moment(values[1]).format("DD MMM yyyy HH:mm"));

  //   setTotalHours(1);
  // }
  function selectTimeSlots() {
    setFrom(moment().format("DD MMM yyyy HH:mm"));
    setTo(moment().format("DD MMM yyyy HH:mm"));

    setTotalHours(1);
  }


  function onToken(token) {
    const reqObj = {
      token,
      user: JSON.parse(localStorage.getItem("user"))._id,
      business: business._id,
      totalHours,
      totalAmount,
      priorityRequired: priority,
      queuedTimeSlots: {
        from,
        to,
      },
    };

    dispatch(queueBusiness(reqObj));
  }

  return (
    <DefaultLayout>
      <Helmet>
        <style>{'body { background-color: black; }'}</style>
      </Helmet>
      {loading && <Spinner />}
      <Row
        justify="center"
        className="d-flex align-items-center"
        style={{ minHeight: "90vh" }}
      >
        <Col lg={10} sm={24} xs={24} className='p-3'>
          <img src={business.image} className="businessimg2 bs1 w-100" data-aos='flip-left' data-aos-duration='1500' alt=""/>
        </Col>

        <Col lg={10} sm={24} xs={24} className="text-right">
          <Divider style={{ color: "#4299c1" }} type="horizontal">
            Business Info
          </Divider>
          <div style={{ textAlign: "right", color: "#4299c1" }}>
            <p>Business: {business.name}</p>
            <p>Deposit Amount: {business.depositAmount} </p>
            <p>Business Type : {business.businessType}</p>
          </div>

          <Divider style={{ color: "#4299c1" }} type="horizontal">
            Select Time Slots
          </Divider>


          <DatePicker
            className="datePicker"
            showTime={{ format: "HH:mm" }}
            format="DD MMM yyyy HH:mm"
            onChange={selectTimeSlots}
          //   style={{
          //     borderColor: '#4299c1', //changing the footer colour only
          //     backgroundColor: 'white', //not working
          //     color: 'black'
          // }}
          />
          {/* <RangePicker
            showTime={{ format: "HH:mm" }}
            format="DD MMM yyyy HH:mm"
            onChange={selectTimeSlots}
          /> */}
          <br />

          {/* <button
            className="btn1 mt-2"
            onClick={() => {
              setShowModal(true);
            }}
          >
            See Queue Slots
          </button> */}

          {from && to && (
            <div style={{ color: "#4299c1" }}>
              {/* <p>
                Total Hours : <b>{totalHours}</b>
              </p> */}
              <p>
                Deposit Amount : <b>{business.depositAmount}</b>
              </p>
              <Checkbox style={{ color: "#4299c1" }}
                onChange={(e) => {
                  if (e.target.checked) {
                    setpriority(true);
                  } else {
                    setpriority(false);
                  }
                }}
              >
                Priority Required
              </Checkbox>

              <h3 style={{ color: '#4299c1' }} >Total Amount : ${totalAmount / 100}</h3>

              <StripeCheckout
                shippingAddress
                token={onToken}
                currency='sgd'
                amount={totalAmount}
                stripeKey="pk_test_GvF3BSyx8RSXMK5yAFhqEd3H"
              >
                <button className="btn1">
                  Queue Now
                </button>
              </StripeCheckout>


            </div>
          )}
          <br></br>
          <br></br>

          <div className='text-right'>
            <Link to="/">Back</Link>
            <span>   </span>
          </div>
        </Col>

        {business.name && (
          <Modal
            visible={showModal}
            closable={false}
            footer={false}
            title="Queued time slots"
            classNames={{ overlay: { background: 'red' } }}
          >
            <div className="p-2">
              {business.queuedTimeSlots.map((slot) => {
                return (
                  <button className="btn1 mt-2">
                    {slot.from} - {slot.to}
                  </button>
                );
              })}

              <div className="text-right mt-5">
                <button
                  className="btn1"
                  onClick={() => {
                    setShowModal(false);
                  }}
                >
                  CLOSE
                </button>
              </div>
            </div>
          </Modal>
        )}
      </Row>
    </DefaultLayout>
  );
}

export default QueuingBusiness;
